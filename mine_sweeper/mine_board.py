import cv2
import numpy as np
import pyautogui
import random


"""
Board value:
    0: blank
    1 ~ 9: number 1 ~ 9
    10: unknown
    11: flag
"""

class Board:
    def __init__(self, isDebug = False, isMegaDebugging = False):
        self.temp_0 = cv2.imread("0.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_1 = cv2.imread("1.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_2 = cv2.imread("2.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_3 = cv2.imread("3.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_4 = cv2.imread("4.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_5 = cv2.imread("5.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_6 = cv2.imread("6.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_7 = cv2.imread("7.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_8 = cv2.imread("8.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_flag = cv2.imread("flag.jpg", cv2.IMREAD_UNCHANGED)
        self.temp_unknown = cv2.imread("unknown.jpg", cv2.IMREAD_UNCHANGED)
        self.board_size = (9, 9)
        self.isDebug = isDebug or isMegaDebugging
        self.isMegaDebugging = isMegaDebugging

    def print_board(self):
        print(self.board)
        
    def get_board(self):
        return self.board
    
    def update(self, image, window_position):
        self.window_position = window_position
        self.board = np.zeros(self.board_size)
        self.update_board(image)
        if self.isDebug:
            cv2.waitKey(0)
        else:
            self.board_event()

    def board_event(self):
        print ("===============")
        for i, row in enumerate(self.board):
            for j, cell in enumerate(row):
                if cell in range(1, 9):
                    adjs = self.get_adjacent(j, i)
                    n_unknows = [k for k in adjs if k[0] == -1]
                    n_flags = [k for k in adjs if k[0] == 10]
                    if len(n_unknows) != 0 and len(n_unknows) + len(n_flags) == cell:
                        position = n_unknows[0][1]
                        print(position)
                        self.click_r(*position)
                        return
                    elif len(n_unknows) != 0 and len(n_flags) == cell:
                        position = n_unknows[0][1]
                        print(position)
                        self.click_l(*position)
                        return
        self.click_random()

    def get_adjacent(self, x, y):
        result = []
        positions = [
            [x-1, y-1], [x  , y-1], [x+1, y-1], 
            [x-1, y  ],             [x+1, y  ], 
            [x-1, y+1], [x  , y+1], [x+1, y+1]]
        for position in positions:
            x, y = position
            if x >=0 and y >=0:
                try:
                    result.append([self.board[y][x], position])
                except IndexError:
                    pass
        return result

    def click_l(self, x, y):
        x_res = self.window_position[0] + self.offset[0] + self.cell_size[0] * (x + 0.5)
        y_res = self.window_position[1] + self.offset[1] + self.cell_size[1] * (y + 0.5)
        pyautogui.click(x = x_res, y = y_res)
        
    def click_r(self, x, y):
        x_res = self.window_position[0] + self.offset[0] + self.cell_size[0] * (x + 0.5)
        y_res = self.window_position[1] + self.offset[1] + self.cell_size[1] * (y + 0.5)
        pyautogui.rightClick(x = x_res, y = y_res)

    def click_random(self):
        unknowns = []
        for i, row in enumerate(self.board):
            for j, cell in enumerate(row):
                if cell == -1:
                    unknowns.append([cell, [j, i]])
        selected = unknowns[random.randrange(0, len(unknowns))]
        self.click_l(*(selected[1]))

    def update_board(self, image):
        self.update_offset(image)
        if self.isDebug:
            self.draw_board(image)
        self.set_value(1, image, self.temp_1, 0.7, isDebug = self.isDebug)
        self.set_value(2, image, self.temp_2, 0.6, isDebug = self.isMegaDebugging)
        self.set_value(3, image, self.temp_3, 0.6, isDebug = self.isMegaDebugging)
        self.set_value(4, image, self.temp_4, 0.7, isDebug = self.isMegaDebugging)
        self.set_value(5, image, self.temp_5, 0.7, isDebug = self.isMegaDebugging)
        #flag is 11
        self.set_value(10, image, self.temp_flag, 0.5, isDebug = self.isDebug)
        self.set_unknown(image)

    def update_offset(self, image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)

        contours = None
        if cv2.__version__ == "4.0.0":
            contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        else:
            _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        rects = [cv2.boundingRect(contour) for contour in contours if cv2.contourArea(contour) > 1500]
        rects = [rect for rect in rects if rect[0] > 20 and rect[1] > 20]
        rects.sort(key = lambda x: x[0] + x[1])
        leftTop = rects[0]
        cell_size = rects[1][0] - rects[0][0] if rects[1][0] - rects[0][0] > 20 else rects[1][1] - rects[0][1]
        self.offset = (leftTop[0], leftTop[1])
        self.cell_size = (cell_size, cell_size)

    def draw_board(self, image):
        board = image.copy()
        for i in range(9):
            for j in range(9):
                x = self.offset[0] + j * self.cell_size[0]
                y = self.offset[1] + i * self.cell_size[1]
                cv2.rectangle(board, (x, y), (x+self.cell_size[0], y+self.cell_size[1]), (0, 0, 255), 3)
        cv2.imshow("board", board)
    
    def set_unknown(self, image):
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        lowerbound = np.array([90, 120, 255])
        upperbound = np.array([110, 160, 255])
        mask = cv2.inRange(hsv, lowerbound, upperbound)
        contours = None
        if cv2.__version__ == "4.0.0":
            contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        else:
            _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        rects = [cv2.boundingRect(contour) for contour in contours if cv2.contourArea(contour) > 10]
        
        for rect in rects:
            x, y, w, h = rect
            x, y = self.find_position((x + w // 2, y + h // 2))
            self.board[y][x] = -1

        if self.isDebug:
            debugging_image = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
            for rect in rects:
                x, y, w, h = rect
                cv2.rectangle(debugging_image, (x, y), (x+w, y+h), (0, 255, 0), 3)
                # cv2.circle(debugging_image, (x + w // 2, y + h // 2), 5, (0, 255, 0), 3)
            cv2.imshow('unknown', debugging_image)
    
    def set_value(self, value, image, template, threshold, isDebug = False):
        _, w, h = template.shape[::-1]
    
        res = cv2.matchTemplate(image, template, cv2.TM_CCOEFF_NORMED)
        loc = np.where( res >= threshold )
        for point in zip(*loc[::-1]):
            x, y = self.find_position((point[0] + w // 2, point[1] + h // 2))
            #print((x, y))
            self.board[y][x] = value
        
        if isDebug:
            debugging_image = image.copy()
            for point in zip(*loc[::-1]):
                cv2.rectangle(debugging_image, point, (point[0] + w, point[1] + h), (255, 0, 0), 3)
            cv2.imshow(str(value), debugging_image)
            
    def find_position(self, point):
        x = int(point[0] - self.offset[0]) // self.cell_size[0]
        y = int(point[1] - self.offset[1]) // self.cell_size[1]
        return (x, y)
        '''
    def board_controll(self):
        for i in range (0, 9):
            for j in range (0, 9):
                if (self.board[i][j] > 0 && self.board[i][j] != 10):
                    return self.borad_number(self.board[i][j], i, j)
                    
                    
    def board_number(self, n, i, j):
        adjs = this.get_adjacent(i, j)
        unknown_counter = 0
        flag_counter = 0
        for i in adjs:
            if (i == -1):
                unknown_counter + 1
            else if (i == 10):
                flag_counter + 1
        if (unknown_counter - flag_counter = n):
            for i in adjs:
                if (i )
        else if (flag_counter = n):
            '''
    