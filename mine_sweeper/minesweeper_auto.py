import pyautogui
import numpy as np
import cv2
from PIL import ImageGrab
import win32gui
import win32api
import win32con

from mine_board import Board

toplist, winlist = [], []
def enum_cb(hwnd, results):
        winlist.append((hwnd, win32gui.GetWindowText(hwnd)))

def init():
    win32gui.EnumWindows(enum_cb, toplist)
    windows = [(hwnd, title) for hwnd, title in winlist if 'microsoft minesweeper' in title.lower()]
    # just grab the hwnd for first window matching firefox
    return windows[0][0]
    
def screenshot(hwnd):
    win32gui.SetForegroundWindow(hwnd)
    bbox = win32gui.GetWindowRect(hwnd)
    image = np.array(ImageGrab.grab(bbox))
    return bbox, cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    
if __name__ == "__main__":
    isDebug = False
    isMegaDebugging = False
    board = Board(isDebug = isDebug, isMegaDebugging = isMegaDebugging)
    move = 30
    if isDebug or isMegaDebugging:
        move = 1

    while True:
        hwnd = init()
        bbox, image = screenshot(hwnd)
        print(bbox)
        board.update(image, (bbox[0], bbox[1]))
        board.print_board()
        if isDebug or isMegaDebugging:
            break
        cv2.waitKey(100)