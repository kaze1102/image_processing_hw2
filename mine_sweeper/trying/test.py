import cv2

image = cv2.imread("test.jpg", cv2.IMREAD_UNCHANGED)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
_, mask = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)

contours = None
if cv2.__version__ == "4.0.0":
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
else:
    _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
rects = [cv2.boundingRect(contour) for contour in contours if cv2.contourArea(contour) > 2000]
rects = [rect for rect in rects if rect[0] > 20 and rect[1] > 20]
rects.sort(key = lambda x: x[0] + x[1])
debugging_image = image.copy()
for rect in rects:
    x, y, w, h = rect
    cv2.rectangle(debugging_image, (x, y), (x+w, y+h), (0, 255, 0), 3)
    # cv2.circle(debugging_image, (x + w // 2, y + h // 2), 5, (0, 255, 0), 3)
x, y, w, h = rects[0]
cv2.rectangle(debugging_image, (x, y), (x+w, y+h), (255, 0, 0), 3)
x, y, w, h = rects[1]
cv2.rectangle(debugging_image, (x, y), (x+w, y+h), (255, 255, 0), 3)
cv2.imshow('unknown', debugging_image)
cv2.waitKey(0)