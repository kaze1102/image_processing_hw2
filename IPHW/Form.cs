﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.OCR;
using Emgu.CV.Structure;


namespace IPHW
{
	public partial class Form : System.Windows.Forms.Form
	{
		VideoCapture _capture;
        Tesseract _tesseract;
		private Image<Bgr, Byte> _camImage = null;

        private Point RectStartPoint;
        private Rectangle _selectedRegion = new Rectangle();
        private Brush selectionBrush = new SolidBrush(Color.FromArgb(128, 72, 145, 220));

        bool isInOcrMode = false;

        // Start Rectangle
        //
        private void SourcePictureBoxMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Determine the initial rectangle coordinates...
            if (!isInOcrMode)
                return;
            RectStartPoint = e.Location;
            Invalidate();
        }

        // Draw Rectangle
        //
        private void SourcePictureBoxMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!isInOcrMode || e.Button != MouseButtons.Left)
                return;
            Point tempEndPoint = e.Location;
            _selectedRegion.Location = new Point(
                Math.Min(RectStartPoint.X, tempEndPoint.X),
                Math.Min(RectStartPoint.Y, tempEndPoint.Y));
            _selectedRegion.Size = new Size(
                Math.Abs(RectStartPoint.X - tempEndPoint.X),
                Math.Abs(RectStartPoint.Y - tempEndPoint.Y));
            _sourcePictureBox.Invalidate();
        }

        // Draw Area
        //
        private void SourcePictureBoxPaint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Draw the rectangle...
            if (!isInOcrMode)
                return;
            if (_sourcePictureBox.Image != null)
            {
                if (_selectedRegion != null && _selectedRegion.Width > 0 && _selectedRegion.Height > 0)
                {
                    e.Graphics.FillRectangle(selectionBrush, _selectedRegion);
                }
            }
        }

        private void SourcePictureBoxMouseUp(object sender, MouseEventArgs e)
        {
            if (!isInOcrMode)
                return;
            _messageBox.Text = "Orc result: " + OcrDetect();
            _selectedRegion = new Rectangle();
            _sourcePictureBox.Invalidate();
        }

        public Form()
		{
			InitializeComponent();
            _messageBox.Text = "";
            DisableAllButton();
            string path = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName).FullName + @"\tessdata";
            //string path = @"D:\QQ\College\ImageProcessing\image_processing_hw2\IPHW\tessdata";
            _tesseract = new Tesseract(path , "eng", OcrEngineMode.Default);
        }

		private void ClickOpenCameraButton(object sender, EventArgs e)
		{
			if (_capture == null)
			{
				_capture = new VideoCapture(0);
                if (!_capture.IsOpened)
                {
                    _messageBox.Text = "無法開啟攝影機";
                    return;
                }
                _messageBox.Text = "";
                EnableAllButton();
                Application.Idle += ProcessFrame;
                (sender as Button).Text = "關閉攝影機";
            }

			else if (_capture != null)
            {
                _capture.Dispose();
                _capture = null;
                DisableAllButton();
                Application.Idle -= ProcessFrame;
                (sender as Button).Text = "開啟攝影機";
            }
                
		}

        Image<Bgr, Byte> applyEffect(Image<Bgr, Byte> image)
        {
            if (_sobelButton.Enabled == false)
                return applySobel(image);
            else if (_grayButton.Enabled == false)
                return toGray(image);
            else if (_thresholdButton.Enabled == false)
            {
                Image<Bgr, Byte> result = image.Copy();
                result._ThresholdBinary(new Bgr(127, 127, 127), new Bgr(255, 255, 255));
                return result;
            }
            else if (_sharpenButton.Enabled == false)
            {
                ConvolutionKernelF filter = new ConvolutionKernelF(new float[3, 3]{
                    { 0,-1, 0 },
                    {-1, 5,-1 },
                    { 0,-1, 0 }
                });
                return applyFilter(image, filter);
                
            }
            else if (_invertButton.Enabled == false)
                return new Bgr(255, 255, 255) - image;
            else if (_smoothButton.Enabled == false)
            {
                Image<Bgr, Byte> result = image.Copy();
                result._SmoothGaussian(25);
                return result;
            }
            else
                return image;
        }

        Image<Bgr, Byte> applySobel(Image<Bgr, Byte> image)
        {
            Image<Gray, Byte> grayImage = image.Convert<Gray, Byte>();
            //horizontal filter
            Image<Gray, float> sobelX = grayImage.Sobel(1, 0, 3);
            //vertical filter
            Image<Gray, float> sobelY = grayImage.Sobel(0, 1, 3);

            //Convert negative values to positive valus
            sobelX = sobelX.AbsDiff(new Gray(0));
            sobelY = sobelY.AbsDiff(new Gray(0));

            Image<Gray, float> sobel = sobelX + sobelY;
            //Find sobel min or max value
            double[] mins, maxs;
            //Find sobel min or max value position
            Point[] minLoc, maxLoc;
            sobel.MinMax(out mins, out maxs, out minLoc, out maxLoc);
            //Conversion to 8-bit image
            Image<Gray, Byte> sobelImage = sobel.ConvertScale<byte>(255 / maxs[0], 0);

            return sobel.Convert<Bgr, Byte>();
        }

        Image<Bgr, Byte> toGray(Image<Bgr, Byte> image)
        {
            return image.Convert<Gray, Byte>().Convert<Bgr, Byte>();
        }

        Image<Bgr, Byte> applyFilter(Image<Bgr, Byte> image, ConvolutionKernelF filter)
        {
            Image<Bgr, Byte> result = new Image<Bgr, Byte>(image.Width, image.Height);
            CvInvoke.Filter2D(image, result, filter, new Point(0, 0));
            return result;
        }

        string OcrDetect()
        {
            Mat selectedImage = OcrPreprocess();
            _outputPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            _outputPictureBox.Image = selectedImage.Bitmap;
            _tesseract.SetImage(selectedImage);
            _tesseract.Recognize();
            return _tesseract.GetUTF8Text();
        }

        private Mat OcrPreprocess()
        {
            ConvolutionKernelF filter = new ConvolutionKernelF(new float[3, 3]{
                    { 0,-1, 0 },
                    {-1, 5,-1 },
                    { 0,-1, 0 }
                });
            Image<Bgr, Byte> image = applyFilter(_camImage, filter);
            Image<Gray, Byte> gray = image.Convert<Gray, Byte>();
            
            //CvInvoke.AdaptiveThreshold(gray, gray, 255, Emgu.CV.CvEnum.AdaptiveThresholdType.MeanC, Emgu.CV.CvEnum.ThresholdType.Binary, 11, 2);
            //gray = gray.SmoothBlur(3, 3);
            //gray._MorphologyEx(Emgu.CV.CvEnum.MorphOp.Blackhat, new Mat(), Point.Empty, 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
            //gray._MorphologyEx(Emgu.CV.CvEnum.MorphOp.Dilate, new Mat(), Point.Empty, 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
            //gray._MorphologyEx(Emgu.CV.CvEnum.MorphOp.Erode, new Mat(), Point.Empty, 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
            _selectedRegion = MappingSelectionRegion();

            return new Mat(gray.Mat, _selectedRegion);
        }

        private Rectangle MappingSelectionRegion()
        {
            if (_selectedRegion.Left < 0)
                _selectedRegion.X = 0;
            if (_selectedRegion.Top < 0)
                _selectedRegion.Y = 0;
            if (_selectedRegion.Width + _selectedRegion.Left > _sourcePictureBox.Width)
                _selectedRegion.Width = _sourcePictureBox.Width - _selectedRegion.Left;
            if (_selectedRegion.Height + _selectedRegion.Top > _sourcePictureBox.Height)
                _selectedRegion.Height = _sourcePictureBox.Height - _selectedRegion.Top;
            return new Rectangle(_selectedRegion.Left * _camImage.Width / _sourcePictureBox.Width, _selectedRegion.Top * _camImage.Height / _sourcePictureBox.Height,
                _selectedRegion.Width * _camImage.Width / _sourcePictureBox.Width, _selectedRegion.Height * _camImage.Height / _sourcePictureBox.Height);
        }

        private void ProcessFrame(object sender, EventArgs e)
		{
			if (_capture != null && _capture.Ptr != IntPtr.Zero && !isInOcrMode)
			{
				_camImage = _capture.QueryFrame().ToImage<Bgr, Byte>();
				_sourcePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
				_sourcePictureBox.Image = _camImage.Bitmap;

                _outputPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                _outputPictureBox.Image = applyEffect(_camImage).Bitmap;
			}
		}

        private void EnableAllButton()
        {
            _sobelButton.Enabled = true;
            _grayButton.Enabled = true;
            _thresholdButton.Enabled = true;
            _sharpenButton.Enabled = true;
            _invertButton.Enabled = true;
            _smoothButton.Enabled = true;
            _ocrButton.Enabled = true;
        }

        private void DisableAllButton()
        {
            _sobelButton.Enabled = false;
            _grayButton.Enabled = false;
            _thresholdButton.Enabled = false;
            _sharpenButton.Enabled = false;
            _invertButton.Enabled = false;
            _smoothButton.Enabled = false;
            _ocrButton.Enabled = false;
        }

        private void SelectEffect(object sender, EventArgs e)
        {
            EnableAllButton();
            (sender as Button).Enabled = false;
        }

        private void ShowOcrResult(object sender, EventArgs e)
        {
            if (isInOcrMode)
            {
                isInOcrMode = false;
                Application.Idle += ProcessFrame;
                (sender as Button).Text = "OCR";
            }
            else
            {
                _messageBox.Text = "請框選偵測範圍";
                isInOcrMode = true;
                Application.Idle -= ProcessFrame;
                (sender as Button).Text = "Resume";
            }
        }
    }
}
