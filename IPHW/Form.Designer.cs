﻿namespace IPHW
{
    partial class Form
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this._sourcePictureBox = new System.Windows.Forms.PictureBox();
            this._outputPictureBox = new System.Windows.Forms.PictureBox();
            this._openCameraButton = new System.Windows.Forms.Button();
            this._sobelButton = new System.Windows.Forms.Button();
            this._grayButton = new System.Windows.Forms.Button();
            this._thresholdButton = new System.Windows.Forms.Button();
            this._sharpenButton = new System.Windows.Forms.Button();
            this._invertButton = new System.Windows.Forms.Button();
            this._smoothButton = new System.Windows.Forms.Button();
            this._messageBox = new System.Windows.Forms.TextBox();
            this._ocrButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._sourcePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._outputPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // _sourcePictureBox
            // 
            this._sourcePictureBox.Location = new System.Drawing.Point(30, 30);
            this._sourcePictureBox.Name = "_sourcePictureBox";
            this._sourcePictureBox.Size = new System.Drawing.Size(480, 320);
            this._sourcePictureBox.TabIndex = 0;
            this._sourcePictureBox.TabStop = false;
            this._sourcePictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.SourcePictureBoxPaint);
            this._sourcePictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SourcePictureBoxMouseDown);
            this._sourcePictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SourcePictureBoxMouseMove);
            this._sourcePictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SourcePictureBoxMouseUp);
            // 
            // _outputPictureBox
            // 
            this._outputPictureBox.Location = new System.Drawing.Point(552, 30);
            this._outputPictureBox.Name = "_outputPictureBox";
            this._outputPictureBox.Size = new System.Drawing.Size(320, 240);
            this._outputPictureBox.TabIndex = 1;
            this._outputPictureBox.TabStop = false;
            // 
            // _openCameraButton
            // 
            this._openCameraButton.Location = new System.Drawing.Point(30, 368);
            this._openCameraButton.Name = "_openCameraButton";
            this._openCameraButton.Size = new System.Drawing.Size(95, 31);
            this._openCameraButton.TabIndex = 0;
            this._openCameraButton.Text = "開啟攝影機";
            this._openCameraButton.UseVisualStyleBackColor = true;
            this._openCameraButton.Click += new System.EventHandler(this.ClickOpenCameraButton);
            // 
            // _sobelButton
            // 
            this._sobelButton.Location = new System.Drawing.Point(552, 368);
            this._sobelButton.Name = "_sobelButton";
            this._sobelButton.Size = new System.Drawing.Size(95, 31);
            this._sobelButton.TabIndex = 1;
            this._sobelButton.TabStop = false;
            this._sobelButton.Text = "sobel邊緣偵測";
            this._sobelButton.UseVisualStyleBackColor = true;
            this._sobelButton.Click += new System.EventHandler(this.SelectEffect);
            // 
            // _grayButton
            // 
            this._grayButton.Location = new System.Drawing.Point(665, 368);
            this._grayButton.Name = "_grayButton";
            this._grayButton.Size = new System.Drawing.Size(95, 31);
            this._grayButton.TabIndex = 2;
            this._grayButton.TabStop = false;
            this._grayButton.Text = "灰階";
            this._grayButton.UseVisualStyleBackColor = true;
            this._grayButton.Click += new System.EventHandler(this.SelectEffect);
            // 
            // _thresholdButton
            // 
            this._thresholdButton.Location = new System.Drawing.Point(777, 368);
            this._thresholdButton.Name = "_thresholdButton";
            this._thresholdButton.Size = new System.Drawing.Size(95, 31);
            this._thresholdButton.TabIndex = 0;
            this._thresholdButton.TabStop = false;
            this._thresholdButton.Text = "二值化";
            this._thresholdButton.UseVisualStyleBackColor = true;
            this._thresholdButton.Click += new System.EventHandler(this.SelectEffect);
            // 
            // _sharpenButton
            // 
            this._sharpenButton.Location = new System.Drawing.Point(552, 442);
            this._sharpenButton.Name = "_sharpenButton";
            this._sharpenButton.Size = new System.Drawing.Size(95, 31);
            this._sharpenButton.TabIndex = 4;
            this._sharpenButton.TabStop = false;
            this._sharpenButton.Text = "銳化";
            this._sharpenButton.UseVisualStyleBackColor = true;
            this._sharpenButton.Click += new System.EventHandler(this.SelectEffect);
            // 
            // _invertButton
            // 
            this._invertButton.Location = new System.Drawing.Point(665, 442);
            this._invertButton.Name = "_invertButton";
            this._invertButton.Size = new System.Drawing.Size(95, 31);
            this._invertButton.TabIndex = 5;
            this._invertButton.TabStop = false;
            this._invertButton.Text = "負片";
            this._invertButton.UseVisualStyleBackColor = true;
            this._invertButton.Click += new System.EventHandler(this.SelectEffect);
            // 
            // _smoothButton
            // 
            this._smoothButton.Location = new System.Drawing.Point(777, 442);
            this._smoothButton.Name = "_smoothButton";
            this._smoothButton.Size = new System.Drawing.Size(95, 31);
            this._smoothButton.TabIndex = 6;
            this._smoothButton.TabStop = false;
            this._smoothButton.Text = "高斯模糊";
            this._smoothButton.UseVisualStyleBackColor = true;
            this._smoothButton.Click += new System.EventHandler(this.SelectEffect);
            // 
            // _messageBox
            // 
            this._messageBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._messageBox.Location = new System.Drawing.Point(151, 377);
            this._messageBox.Name = "_messageBox";
            this._messageBox.ReadOnly = true;
            this._messageBox.Size = new System.Drawing.Size(359, 15);
            this._messageBox.TabIndex = 7;
            this._messageBox.TabStop = false;
            this._messageBox.Text = "This is a message box";
            // 
            // _ocrButton
            // 
            this._ocrButton.Location = new System.Drawing.Point(777, 507);
            this._ocrButton.Name = "_ocrButton";
            this._ocrButton.Size = new System.Drawing.Size(95, 31);
            this._ocrButton.TabIndex = 8;
            this._ocrButton.TabStop = false;
            this._ocrButton.Text = "OCR";
            this._ocrButton.UseVisualStyleBackColor = true;
            this._ocrButton.Click += new System.EventHandler(this.ShowOcrResult);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 702);
            this.Controls.Add(this._ocrButton);
            this.Controls.Add(this._messageBox);
            this.Controls.Add(this._smoothButton);
            this.Controls.Add(this._invertButton);
            this.Controls.Add(this._sharpenButton);
            this.Controls.Add(this._thresholdButton);
            this.Controls.Add(this._grayButton);
            this.Controls.Add(this._sobelButton);
            this.Controls.Add(this._openCameraButton);
            this.Controls.Add(this._outputPictureBox);
            this.Controls.Add(this._sourcePictureBox);
            this.Name = "Form";
            this.Text = "HW1";
            ((System.ComponentModel.ISupportInitialize)(this._sourcePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._outputPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox _sourcePictureBox;
        private System.Windows.Forms.PictureBox _outputPictureBox;
        private System.Windows.Forms.Button _openCameraButton;
        private System.Windows.Forms.Button _sobelButton;
        private System.Windows.Forms.Button _grayButton;
        private System.Windows.Forms.Button _thresholdButton;
        private System.Windows.Forms.Button _sharpenButton;
        private System.Windows.Forms.Button _invertButton;
        private System.Windows.Forms.Button _smoothButton;
        private System.Windows.Forms.TextBox _messageBox;
        private System.Windows.Forms.Button _ocrButton;
    }
}

